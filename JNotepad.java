/*
*******************************************************************************
*
* file                            JNotepad.java
*
* discription                     contains class JNotepad
*
* creation date                   03/02/2008
*
* last modified                   16/02/2008
*
* author                          Phil Tsarik
*
*******************************************************************************
*/


/*
*******************************************************************************
*                                 IMPORTS
*******************************************************************************
*/
import javax.swing.*;

/*
*******************************************************************************
*                                 CLASS JNotepad
*******************************************************************************
*/
public class JNotepad
{
	//////////////////////////////////
	// PUBLIC METHODS
	//
	
	//
	// main method
	//
	public static void main(String[] args)
	{
		FrmMain frmMain = new FrmMain();
		frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			SwingUtilities.updateComponentTreeUI(frmMain);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		frmMain.show();
	}
}
