/*
*******************************************************************************
*
* file                            FrmMain.java
*
* discription                     contains class FrmMain
*
* creation date                   03/02/2008
*
* last modified                   23/02/2008
*
* author                          Phil Tsarik
*
*******************************************************************************
*/


/*
*******************************************************************************
*                                 IMPORTS
*******************************************************************************
*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;


/*
*******************************************************************************
*                                 CLASS FrmMain
*******************************************************************************
*/
public class FrmMain extends JFrame
{
	///////////////////////////////////////////////////////
	// PUBLIC VARIBLES
	//
	
	public static final int DEFAULT_WIDTH = 600;       // default window width
	public static final int DEFAULT_HEIGHT = 500;      // default window height
	
	
	///////////////////////////////////////////////////////
	// CONSTRUCTOR
	//
	
	FrmMain()
	{
		// add window listener
		FrmMain.this.addWindowListener(new MyWindowAdapter());
		// init some variables
		currentFile = "Untitled";
		searchingText = "";
		changed = false;
		// get screen size
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension scrSize = kit.getScreenSize();
		int scrWidth = scrSize.width;
		int scrHeight = scrSize.height;
		// set window size
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		// set window position in the center of the screen
		setLocation(scrWidth/2 - DEFAULT_WIDTH/2, scrHeight/2 - DEFAULT_HEIGHT/2);
		// set window icon
		setIconImage(kit.getImage("resources/images/main.gif"));
		// set window title
		setTitle(currentFile+" - JNotepad");
		// create components
		createComponents();
	}
	
	
	///////////////////////////////////////////////////////
	// PRIVATE VARIBLES
	//
	
	private ExtTextArea textArea;                      // component textArea
	private JScrollPane scrollPane;                    // component scrollPane
	private JCheckBoxMenuItem wrapItem;                // component wrapItem
	private JFileChooser fileChooser;                  // component fileChooser
	private MyDocumentListener myDocumentListener;     // component myDocumentListener
	private String currentFile;                        // path of current opened file ("Untitled" if nothing is opened)
	private String searchingText;                      // specified string in Find action
	private boolean changed;                           // changed document flag
	
	
	///////////////////////////////////////////////////////
	// PRIVATE METHODS
	//
	
	//
	// creates components
	//
	private void createComponents()
	{
		// menu File
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		// item New
		JMenuItem newItem = new JMenuItem("New", new ImageIcon("resources/images/new.gif"));
		newItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					execNew();
				}
			});
		newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		newItem.setMnemonic('N');
		fileMenu.add(newItem);
		// item Open
		JMenuItem openItem = new JMenuItem("Open...", new ImageIcon("resources/images/open.gif"));
		openItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					execOpen();
				}
			});
		openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		openItem.setMnemonic('O');
		fileMenu.add(openItem);
		// item Save
		JMenuItem saveItem = new JMenuItem("Save", new ImageIcon("resources/images/save.gif"));
		saveItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					execSave();
				}
			});
		saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		saveItem.setMnemonic('S');
		fileMenu.add(saveItem);
		// item Save As
		JMenuItem saveasItem = new JMenuItem("Save As...");
		saveasItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					execSaveAs();
				}
			});
		saveasItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		saveasItem.setMnemonic('A');
		fileMenu.add(saveasItem);
		fileMenu.addSeparator();
		// item Print
		JMenuItem printItem = new JMenuItem("Print...", new ImageIcon("resources/images/print.gif"));
		printItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					try {
						textArea.print();
					}
					catch (java.awt.print.PrinterException e) {
						e.printStackTrace();
					}
				}
			});
		printItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		printItem.setMnemonic('P');
		fileMenu.add(printItem);
		fileMenu.addSeparator();
		// item Exit
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					int res = 0;
					// if text was changed - confirm to save
					if (changed) {
						res = JOptionPane.showConfirmDialog(FrmMain.this, "Save changes to \""+currentFile+"\"?", "JNotepad", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
						if (res == JOptionPane.YES_OPTION ) {
							execSave();
						}
					}
					// if text was not changed or answer was YES or NO - load new document
					if ( (!changed) || (res == JOptionPane.YES_OPTION) || (res == JOptionPane.NO_OPTION) ) {
						System.exit(0);
					}
				}
			});
		exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		exitItem.setMnemonic('x');
		fileMenu.add(exitItem);
		
		// menu Edit
		JMenu editMenu = new JMenu("Edit");
		editMenu.setMnemonic('E');
		// item Cut
		Action aCut = new
			AbstractAction("Cut")
			{
				public void actionPerformed(ActionEvent event)
				{
					textArea.cut();
				}
			};
		aCut.putValue(Action.SMALL_ICON, new ImageIcon("resources/images/cut.gif"));
		aCut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
		aCut.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		editMenu.add(aCut);
		// item Copy
		Action aCopy = new
			AbstractAction("Copy")
			{
				public void actionPerformed(ActionEvent event)
				{
					textArea.copy();
				}
			};
		aCopy.putValue(Action.SMALL_ICON, new ImageIcon("resources/images/copy.gif"));
		aCopy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		aCopy.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		editMenu.add(aCopy);
		// item Paste
		Action aPaste = new
			AbstractAction("Paste")
			{
				public void actionPerformed(ActionEvent event)
				{
					textArea.paste();
				}
			};
		aPaste.putValue(Action.SMALL_ICON, new ImageIcon("resources/images/paste.gif"));
		aPaste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
		aPaste.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
		editMenu.add(aPaste);
		editMenu.addSeparator();
		// item Find
		Action aFind = new
			AbstractAction("Find...")
			{
				public void actionPerformed(ActionEvent event)
				{
					execFind();
				}
			};
		aFind.putValue(Action.SMALL_ICON, new ImageIcon("resources/images/find.gif"));
		aFind.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
		aFind.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_F);
		editMenu.add(aFind);
		// item Find next
		Action aFindNext = new
			AbstractAction("Find Next")
			{
				public void actionPerformed(ActionEvent event)
				{
					execFindNext();
				}
			};
		aFindNext.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		aFindNext.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		editMenu.add(aFindNext);
		// item Replace
		Action aReplace = new
			AbstractAction("Replace...")
			{
				public void actionPerformed(ActionEvent event)
				{
					execReplace();
				}
			};
		aReplace.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		aReplace.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_R);
		editMenu.add(aReplace);
		editMenu.addSeparator();
		// item Select all
		Action aSelAll = new
			AbstractAction("Select All")
			{
				public void actionPerformed(ActionEvent event)
				{
					textArea.selectAll();
				}
			};
		aSelAll.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
		aSelAll.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		editMenu.add(aSelAll);
		
		// menu View
		JMenu viewMenu = new JMenu("View");
		viewMenu.setMnemonic('V');
		// item Word wrap
		wrapItem = new JCheckBoxMenuItem("Word Wrap");
		wrapItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					textArea.setLineWrap(wrapItem.isSelected());
					scrollPane.validate();
				}
			});
		wrapItem.setMnemonic('W');
		wrapItem.setSelected(false);
		viewMenu.add(wrapItem);
		
		// menu Settings
		JMenu settingsMenu = new JMenu("Settings");
		settingsMenu.setMnemonic('S');
		// submenu Theme
		JMenu themeSettingMenu = new JMenu("Theme");
		themeSettingMenu.setMnemonic('T');
		// radio items Metal theme, Motif theme, Windows theme
		ButtonGroup group1 = new ButtonGroup();
		JRadioButtonMenuItem theme1Item = new JRadioButtonMenuItem("Metal");
		theme1Item.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					try {
						UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
						SwingUtilities.updateComponentTreeUI(FrmMain.this);
						SwingUtilities.updateComponentTreeUI(fileChooser);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			});
		theme1Item.setMnemonic('M');
		theme1Item.setSelected(true);
		JRadioButtonMenuItem theme2Item = new JRadioButtonMenuItem("Motif");
		theme2Item.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					try {
						UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
						SwingUtilities.updateComponentTreeUI(FrmMain.this);
						SwingUtilities.updateComponentTreeUI(fileChooser);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			});
		theme2Item.setMnemonic('o');
		JRadioButtonMenuItem theme3Item = new JRadioButtonMenuItem("Windows");
		theme3Item.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					try {
						UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
						SwingUtilities.updateComponentTreeUI(FrmMain.this);
						SwingUtilities.updateComponentTreeUI(fileChooser);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			});
		theme3Item.setMnemonic('W');
		group1.add(theme1Item);
		group1.add(theme2Item);
		group1.add(theme3Item);
		themeSettingMenu.add(theme1Item);
		themeSettingMenu.add(theme2Item);
		themeSettingMenu.add(theme3Item);
		settingsMenu.add(themeSettingMenu);
		
		// menu Help
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');
		// item About
		JMenuItem aboutItem = new JMenuItem("About...");
		aboutItem.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JOptionPane.showMessageDialog(FrmMain.this,"<html><b>JNotepad 1.0</b></html>\n\n"+"<html>&#169; 2008 Phil Tsarik</html>","About",JOptionPane.INFORMATION_MESSAGE);
				}
			});
		aboutItem.setMnemonic('A');
		helpMenu.add(aboutItem);
		
		// place menu bar
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(viewMenu);
		menuBar.add(settingsMenu);
		menuBar.add(helpMenu);
		
		// create popup menu
		JPopupMenu popup = new JPopupMenu();
		popup.add(aCut);
		popup.add(aCopy);
		popup.add(aPaste);
		popup.addSeparator();
		popup.add(aFind);
		popup.add(aFindNext);
		popup.add(aReplace);
		popup.addSeparator();
		popup.add(aSelAll);
		
		// create textArea
		textArea = new ExtTextArea(8,40);
		textArea.setLineWrap(wrapItem.isSelected());
		myDocumentListener = new MyDocumentListener();
		textArea.getDocument().addDocumentListener(myDocumentListener);
		scrollPane = new JScrollPane(textArea);
		// add popup listener
		textArea.addPopupListener(popup);
		
		// create font
		Font font = new Font("Lucida Console", Font.PLAIN, 14);
		textArea.setFont(font);
		
		// create open/save dialog
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));
		
		// place components on frame
		Container contentPane = getContentPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
	}
	
	//
	// executes creating new document
	//
	private void execNew()
	{
		int res = 0;
		// if text was changed - confirm to save
		if (changed) {
			res = JOptionPane.showConfirmDialog(FrmMain.this, "Save changes to \""+currentFile+"\"?", "JNotepad", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (res == JOptionPane.YES_OPTION ) {
				execSave();
			}
		}
		// if text was not changed or answer was YES or NO - load new document
		if ( (!changed) || (res != JOptionPane.CANCEL_OPTION) ) {
			textArea.setText("");
			currentFile = "Untitled";
			setTitle(currentFile+" - JNotepad");
			changed = false;
			textArea.getDocument().removeDocumentListener(myDocumentListener);
			textArea.getDocument().addDocumentListener(myDocumentListener);
		}
	}
	
	//
	// executes opening file
	//
	private void execOpen()
	{
		int res = 0;
		// if text was changed - confirm to save
		if (changed) {
			res = JOptionPane.showConfirmDialog(FrmMain.this, "Save changes to \""+currentFile+"\"?", "JNotepad", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (res == JOptionPane.YES_OPTION ) {
				execSave();
			}
		}
		// if text was not changed or answer was YES or NO - open document
		if ( (!changed) || (res != JOptionPane.CANCEL_OPTION) ) {
			if (fileChooser.showOpenDialog(FrmMain.this) == fileChooser.APPROVE_OPTION)
			{
				if (textArea.readFromFile(fileChooser.getSelectedFile()) == 0) {
					currentFile = fileChooser.getSelectedFile().getName();
					setTitle(currentFile+" - JNotepad");
					changed = false;
					textArea.getDocument().removeDocumentListener(myDocumentListener);
					textArea.getDocument().addDocumentListener(myDocumentListener);
				}
			}
		}
	}
	
	//
	// executes saving file
	//
	private void execSave()
	{
		if (!currentFile.equals("Untitled")) {
			File file = new File(currentFile);
			if (textArea.writeToFile(file) == 0) {
				currentFile = file.getName();
				setTitle(currentFile+" - JNotepad");
				changed = false;
			}
		} else {
			execSaveAs();
		}
	}
	
	//
	// executes saving as file
	//
	private void execSaveAs()
	{
		if (fileChooser.showSaveDialog(FrmMain.this) == fileChooser.APPROVE_OPTION) {
			if (textArea.writeToFile(fileChooser.getSelectedFile()) == 0) {
				currentFile = fileChooser.getSelectedFile().getName();
				setTitle(currentFile+" - JNotepad");
				changed = false;
			}
		}
	}
	
	//
	// executes text search
	//
	private void execFind()
	{
		// create components of the dialog
		Object[] comp = {
			"Search string:",
			new JTextField(textArea.getSelectedText())
		};
		// show the dialog
		int res = JOptionPane.showOptionDialog(
				FrmMain.this,
				comp,
				"Find text",
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				new String[] {"Find", "Close"},
				null
			);
		// analize the result
		switch (res) {
			case 0: // find
				searchingText = ((JTextField)comp[1]).getText();
				if (!searchingText.equals("")) {
					if (textArea.findText(searchingText, 0) != 0) {
						JOptionPane.showMessageDialog(FrmMain.this,"The specified text was not found","Find text",JOptionPane.WARNING_MESSAGE);
						searchingText = "";
					}
				}
				break;
		}
	}
	
	//
	// executes text search next
	//
	private void execFindNext()
	{
		if (searchingText.equals("")) {
			execFind();
		} else {
			if (textArea.findText(searchingText, textArea.getSelectionEnd()) != 0) {
				JOptionPane.showMessageDialog(FrmMain.this,"The specified text was not found","Find text",JOptionPane.WARNING_MESSAGE);
				searchingText = "";
			}
		}
	}
	
	//
	// executes text replace
	//
	private void execReplace()
	{
		// create components of the dialog
		Object[] comp = {
			"Search string:",
			new JTextField(textArea.getSelectedText()),
			"Replace with:",
			new JTextField()
		};
		int res = -2;
		// while Close button or Esc will be not pressed
		while ( (res != 3) && (res != -1) ) {
			// show the dialog
			res = JOptionPane.showOptionDialog(
				FrmMain.this,
				comp,
				"Replace text",
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				new String[] {"Find", "Replace", "Replace All", "Close"},
				null
			);
			// analize the result
			switch (res) {
				case 0: // find
					if (!((JTextField)comp[1]).getText().equals("")) {
						if (textArea.findText(((JTextField)comp[1]).getText(), textArea.getSelectionEnd()) != 0) {
							JOptionPane.showMessageDialog(FrmMain.this,"The specified text was not found","Replace text",JOptionPane.WARNING_MESSAGE);
							textArea.setSelectionStart(0);
							textArea.setSelectionEnd(0);
						}
					}
					break;
				case 1: // replace
					if (!((JTextField)comp[1]).getText().equals("")) {
						if (textArea.replaceText(((JTextField)comp[1]).getText(), ((JTextField)comp[3]).getText(), textArea.getSelectionStart()) != 0) {
							JOptionPane.showMessageDialog(FrmMain.this,"The specified text was not found","Replace text",JOptionPane.WARNING_MESSAGE);
							textArea.setSelectionStart(0);
							textArea.setSelectionEnd(0);
						}
					}
					break;
				case 2: // replace all
					if (!((JTextField)comp[1]).getText().equals("")) {
						if (textArea.replaceAllText(((JTextField)comp[1]).getText(), ((JTextField)comp[3]).getText(), 0) != 0) {
							JOptionPane.showMessageDialog(FrmMain.this,"The specified text was not found","Replace text",JOptionPane.WARNING_MESSAGE);
							textArea.setSelectionStart(0);
							textArea.setSelectionEnd(0);
						}
					}
					break;
			}
		}
	}
	
	
	///////////////////////////////////////////////////////
	// PRIVATE CLASSES
	//
	
	//
	// realizes receiving document events
	//
	private class MyDocumentListener implements DocumentListener
	{
		public void changedUpdate(DocumentEvent e)
		{
			setTitle("* "+currentFile+" - JNotepad");
			changed = true;
		}
		public void insertUpdate(DocumentEvent e)
		{
			setTitle("* "+currentFile+" - JNotepad");
			changed = true;
		}
		public void removeUpdate(DocumentEvent e)
		{
			setTitle("* "+currentFile+" - JNotepad");
			changed = true;
		}
	}
	
	//
	// realizes receiving window events
	//
	private class MyWindowAdapter extends WindowAdapter
	{
		public void windowClosing(WindowEvent e)
		{
			int res = 0;
			// if text was changed - confirm to save
			if (changed) {
				res = JOptionPane.showConfirmDialog(FrmMain.this, "Save changes to \""+currentFile+"\"?", "JNotepad", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (res == JOptionPane.YES_OPTION ) {
					execSave();
				}
			}
			// if text was not changed or answer was YES or NO - load new document
			if ( (!changed) || (res == JOptionPane.YES_OPTION) || (res == JOptionPane.NO_OPTION) ) {
				FrmMain.this.setDefaultCloseOperation(EXIT_ON_CLOSE);
				return;
			}
			// if CANCEL button or Esc was pressed
			FrmMain.this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		}
	}
}
