/*
*******************************************************************************
*
* file                            ExtTextArea.java
*
* discription                     contains class ExtTextArea
*
* creation date                   04/02/2008
*
* last modified                   16/02/2008
*
* author                          Phil Tsarik
*
*******************************************************************************
*/


/*
*******************************************************************************
*                                 IMPORTS
*******************************************************************************
*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

/*
*******************************************************************************
*                                 CLASS ExtTextArea
*******************************************************************************
*/
public class ExtTextArea extends JTextArea
{
	///////////////////////////////////////////////////////
	// CONSTRUCTOR
	//
	
	ExtTextArea(int rows, int columns)
	{
		super(rows, columns);
		pl = new ETAPopupListener();
	}
	
	
	
	///////////////////////////////////////////////////////
	// PUBLIC METHODS
	//
	
	//
	// searches text in JTextArea
	//
	// String str                  text to search
	// int from                    index from which to search
	//
	public int findText(String str, int from)
	{
		int startIndex = ExtTextArea.this.getText().indexOf(str, from);
		if (startIndex != -1) {
			ExtTextArea.this.select(startIndex, startIndex+str.length());
			return 0;
		}
		return 1;
	}
	
	//
	// replaces text in JTextArea
	//
	// String str                  text to search
	// String rsrt                 text to replace
	// int from                    index from which to search the text
	//
	public int replaceText(String str, String rsrt, int from)
	{
		int startIndex = ExtTextArea.this.getText().indexOf(str, from);
		if (startIndex != -1) {
			ExtTextArea.this.replaceRange(rsrt, startIndex, startIndex + str.length());
			ExtTextArea.this.select(startIndex, startIndex+rsrt.length());
			return 0;
		}
		return 1;
	}
	
	//
	// replaces all entries in JTextArea
	//
	// String str                  text to search
	// String rsrt                 text to replace
	// int from                    index from which to search the text
	//
	public int replaceAllText(String str, String rsrt, int from)
	{
		int startIndex = ExtTextArea.this.getText().indexOf(str, from);
		if (startIndex != -1) {
			while (startIndex != -1) {
				ExtTextArea.this.replaceRange(rsrt, startIndex, startIndex + str.length());
				ExtTextArea.this.select(startIndex, startIndex+rsrt.length());
				startIndex = ExtTextArea.this.getText().indexOf(str, ExtTextArea.this.getSelectionEnd());
			}
			return 0;
		}
		return 1;
	}
	
	//
	// reads from file
	//
	// File f                      source file
	//
	public int readFromFile(File f)
	{
		int res = 0;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f.getPath()));
			ExtTextArea.this.read(reader,null);
			reader.close();
		}
		catch (IOException e){
			e.printStackTrace();
			res = 1;
		}
		return res;
	}
	
	//
	// writes to file
	//
	// File f                      source file
	//
	public int writeToFile(File f)
	{
		int res = 0;
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(f.getPath()));
			ExtTextArea.this.write(writer);
			writer.close();
		}
		catch (IOException e){
			e.printStackTrace();
			res = 1;
		}
		return res;
	}
	
	public void addPopupListener(JPopupMenu p)
	{
		//ETAPopupListener pl;
		if (p != null) {
			popup = p;
			ExtTextArea.this.addMouseListener(pl);
		}
		else {
			ExtTextArea.this.removeMouseListener(pl);
		}
	}
	
	
	///////////////////////////////////////////////////////
	// PRIVATE VARIBLES
	//
	
	private ETAPopupListener pl;
	private JPopupMenu popup;
	
	
	///////////////////////////////////////////////////////
	// PRIVATE CLASSES
	//
	
	//
	// realizes receiving mouse events
	//
	private class ETAPopupListener extends MouseAdapter
	{
		public void mousePressed(MouseEvent e)
		{
			// if right mouse button was pressed
			if (e.isPopupTrigger()) {
				// show popup-menu
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
		public void mouseReleased(MouseEvent e)
		{
			// if right mouse button was pressed
			if (e.isPopupTrigger()) {
				// show popup-menu
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}
}